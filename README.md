# Project Model Changes Listener

This BitBucket plugin keeps configuration of [Webhook to Jenkins for Bitbucket](https://marketplace.atlassian.com/plugins/com.nerdwin15.stash-stash-webhook-jenkins/server/overview) 
aligned with `<ciManagement>` section in `pom.xml`. See detailed functionality description in [MarketPlace](https://marketplace.atlassian.com/plugins/com.jv-ration.bitbucket.addon.model-change-listener/server/overview)

## Required Installations

* Open https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK and install Atlassian SDK (does not require admin rights)
* Get `bin` folder of the installed SDK on your PATH

## Running BitBucket locally

Here are the SDK commands you'll use immediately:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-cli   -- after atlas-run or atlas-debug, opens a Maven command line window:
  * 'pi' re-installs the plugin into the running product instance
* atlas-help  -- prints description for all commands in the SDK

Executing `atlas-run --plugins com.nerdwin15.stash:stash-webhook-jenkins:3.0.1` will start BitBucket with PM Changes 
Listener plugin and "Webhook to Jenkins for Bitbucket" installed

Execution of SDK commands requires `M2_HOME` environment variable to point to Maven, which comes with Atlassian SDK. Writing a batch file, which sets `M2_HOME`
and `PATH`, is a good idea.

Full documentation is always available at: https://developer.atlassian.com/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK

## Online Documentation and Forums

Sources used for developing this plugin:
* Using [Event Listeners](https://developer.atlassian.com/bitbucket/server/docs/latest/how-tos/responding-to-application-events.html)
* Using Atlassian SDK and [getting started](https://developer.atlassian.com/docs/getting-started/set-up-the-atlassian-plugin-sdk-and-build-a-project/install-the-atlassian-sdk-on-a-windows-system) with plugin development
* Bitbucket [Server Docs](https://developer.atlassian.com/bitbucket/server/docs/latest/)
* Overview of [libs to use](https://developer.atlassian.com/bitbucket/server/docs/latest/reference/java-api.html)
   * BitBucket Server [JAVA API](https://developer.atlassian.com/static/javadoc/bitbucket-server/4.11.2/api/reference/classes.html)
* BitBucket [Development Community](https://community.developer.atlassian.com/c/bitbucket-development) forum


## Tips & Tricks
* appending `?web.items&web.panels&web.sections` to an URL of running BB displays sections of the page
* for testing with various version of BB use https://developer.atlassian.com/docs/developer-tools/working-with-the-sdk/command-reference/atlas-run-standalone. 
Examples: 
  * `atlas-run-standalone --product bitbucket --version 4.7.1 --data-version 4.7.1 -u 6.2.11`
  * `atlas-run-standalone --product bitbucket --version 5.3.1 --data-version 5.3.1 -u 6.3.1`

## Changes to the generated code
* generic maven properties in pom.xml
* atlassian profile in settings.xml, which adds Atlassian public maven repository
