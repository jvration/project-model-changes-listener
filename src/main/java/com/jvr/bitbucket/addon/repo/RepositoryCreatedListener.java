/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon.repo;

import com.atlassian.bitbucket.event.repository.RepositoryCreatedEvent;
import com.atlassian.bitbucket.hook.repository.RepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.event.api.EventListener;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.bitbucket.project.ProjectType.NORMAL;
import static com.jvr.bitbucket.addon.Constants.POST_RECEIVE_HOOK;

/**
 * Enables the hook for newly created repositories
 */
@Component("repositoryCreatedListener")
public class RepositoryCreatedListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(RepositoryCreatedListener.class);

    @ComponentImport
    private RepositoryHookService rhService;

    @Autowired
    public void setRhService(RepositoryHookService rhService) {
        this.rhService = rhService;
    }

    @EventListener
    public void repositoryCreated(RepositoryCreatedEvent evt) {

        Repository repository = evt.getRepository();
        if (NORMAL == repository.getProject().getType()) {

            RepositoryHook rh = rhService.enable(repository, POST_RECEIVE_HOOK);
            if (rh.isEnabled()) {
                LOGGER.info("{} is enabled for {} repository", POST_RECEIVE_HOOK, repository.getName());
            } else {
                LOGGER.error("{} did not get enabled for {} repository", POST_RECEIVE_HOOK, repository.getName());
            }

        }

    }

}
