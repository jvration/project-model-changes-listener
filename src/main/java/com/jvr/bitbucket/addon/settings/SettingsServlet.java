/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon.settings;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.templaterenderer.RenderingException;
import com.jvr.bitbucket.addon.JvrPluginException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.atlassian.templaterenderer.TemplateRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;

@Component
public class SettingsServlet extends HttpServlet {

    private static final Logger LOGGER = LoggerFactory.getLogger(SettingsServlet.class);

    @ComponentImport
    private transient UserManager userManager;

    @ComponentImport
    private transient LoginUriProvider loginUriProvider;

    @ComponentImport
    private transient TemplateRenderer renderer;

    @Autowired
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    @Autowired
    public void setLoginUriProvider(LoginUriProvider loginUriProvider) {
        this.loginUriProvider = loginUriProvider;
    }

    @Autowired
    public void setRenderer(TemplateRenderer renderer) {
        this.renderer = renderer;
    }

    static boolean isAdmin(UserManager userManager) {
        boolean isAdmin = false;
        UserProfile profile = userManager.getRemoteUser();
        if (profile != null) {
            isAdmin = (profile.getUsername() != null) && userManager.isAdmin(profile.getUserKey());
        }
        return isAdmin;
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {

        if (!isAdmin(userManager)) {
            try {
                redirectToLogin(request, response);
            } catch (IOException e) {
                LOGGER.error("Failed to redirect to login page", e);
            }
            return;
        }

        response.setContentType("text/html;charset=utf-8");
        try (PrintWriter writer = response.getWriter()) {
            renderer.render("vm/settings.vm", writer);
        } catch (RenderingException e) {
            LOGGER.error("Rendering Error occurred in the servlet", e);
        } catch (IOException e) {
            LOGGER.error("IO Error occurred in the servlet", e);
        } catch (JvrPluginException e) {
            LOGGER.error("An execution error detected in the servlet", e);
        }

    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        URI requestUri = getUri(request);
        String loginUri = loginUriProvider.getLoginUri(requestUri).toASCIIString();
        response.sendRedirect(loginUri);
    }

    private URI getUri(HttpServletRequest request) {
        StringBuilder builder = new StringBuilder(request.getRequestURL());
        if (request.getQueryString() != null) {
            builder.append('?');
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

}
