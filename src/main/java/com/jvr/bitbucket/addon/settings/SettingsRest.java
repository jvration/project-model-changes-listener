/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon.settings;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserManager;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.jvr.bitbucket.addon.Constants.KEY_PREFIX;

@Path("/")
@Named("listenerSettingsResource")
public class SettingsRest {

    @ComponentImport
    private UserManager userManager;
    @ComponentImport
    private PluginSettingsFactory pluginSettingsFactory;
    @ComponentImport
    private TransactionTemplate transactionTemplate;

    @Autowired
    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    @Autowired
    public void setPluginSettingsFactory(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @Autowired
    public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
        this.transactionTemplate = transactionTemplate;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context HttpServletRequest request) {

        if (!SettingsServlet.isAdmin(userManager)) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        return Response.ok(transactionTemplate.execute((TransactionCallback<Object>) () -> getPluginConfiguration(pluginSettingsFactory))).build();
    }

    public static ListenerPluginSettings getPluginConfiguration(PluginSettingsFactory pluginSettingsFactory) {

        PluginSettings settings = pluginSettingsFactory.createGlobalSettings();

        ListenerPluginSettings config = new ListenerPluginSettings();

        config.setRepoUrlProtocol((String) settings.get(KEY_PREFIX + "repo.protocol"));
        config.setStandardBranch((String) settings.get(KEY_PREFIX + "branch.st"));
        config.setOtherBranch((String) settings.get(KEY_PREFIX + "branch.oth"));

        config.setDefaults();

        return config;
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(final ListenerPluginSettings config, @Context HttpServletRequest request) {

        if (!SettingsServlet.isAdmin(userManager)) {
            return Response.status(Response.Status.UNAUTHORIZED).build();
        }

        transactionTemplate.execute(() -> {
            PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
            pluginSettings.put(KEY_PREFIX + "repo.protocol", config.getRepoUrlProtocol());
            pluginSettings.put(KEY_PREFIX + "branch.st", config.getStandardBranch());
            pluginSettings.put(KEY_PREFIX + "branch.oth", config.getOtherBranch());
            return null;
        });
        return Response.noContent().build();
    }

}
