/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon.settings;

import org.apache.commons.lang3.StringUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ListenerPluginSettings {

    public static final String PROTOCOL_SSH = "ssh";
    public static final String PROTOCOL_HTTP = "http";

    public static final String BRANCH_MASTER = "master";
    public static final String BRANCH_DEVELOP = "develop";
    public static final String BRANCH_OTHER = "other";

    @XmlElement
    private String repoUrlProtocol;
    @XmlElement
    private String standardBranch;
    @XmlElement
    private String otherBranch;

    /**
     * @return either {@link #PROTOCOL_HTTP} or {@link #PROTOCOL_SSH}
     */
    public String getRepoUrlProtocol() {
        return repoUrlProtocol;
    }

    public String getSavedRepoUrlProtocol() {
        return (StringUtils.isBlank(repoUrlProtocol)) ? PROTOCOL_SSH : repoUrlProtocol;
    }

    public void setRepoUrlProtocol(String repoUrlProtocol) {
        this.repoUrlProtocol = repoUrlProtocol;
    }

    /**
     * @return either {@link #BRANCH_MASTER}, {@link #BRANCH_DEVELOP} or {@link #BRANCH_OTHER}
     */
    public String getStandardBranch() {
        return standardBranch;
    }

    public void setStandardBranch(String standardBranch) {
        this.standardBranch = standardBranch;
    }

    /**
     * @return name of a custom branch to watch. Used only when {@link #getStandardBranch()} returns {@link #BRANCH_OTHER}
     */
    public String getOtherBranch() {
        return otherBranch;
    }

    public void setOtherBranch(String otherBranch) {
        this.otherBranch = otherBranch;
    }

    /**
     * Sets default values to undefined members
     */
    void setDefaults() {
        if (StringUtils.isBlank(repoUrlProtocol)) {
            repoUrlProtocol = PROTOCOL_SSH;
        }
        if (StringUtils.isBlank(standardBranch)) {
            standardBranch = BRANCH_MASTER;
        }
    }

    /**
     * Determines name of the branch to be watched by the plugin
     *
     * @return branch to watch
     */
    public String getBranchToWatch() {
        if (StringUtils.isBlank(standardBranch)) {
            return BRANCH_MASTER;
        } else if (BRANCH_OTHER.equals(standardBranch)) {
            return otherBranch;
        } else {
            return standardBranch;
        }
    }

}
