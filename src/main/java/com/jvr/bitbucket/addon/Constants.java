/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon;

/**
 * @author Viktor Sadovnikov viktor@jv-ration.com
 */
public final class Constants {

    private static final String PLUGIN_KEY = "com.jv-ration.bitbucket.addon.model-change-listener";
    public static final String POST_RECEIVE_HOOK = PLUGIN_KEY + ":" + "jvr-receive-repository-hook";

    public static final String BRANCH_FULL_NAME_PREFIX = "refs/heads/";

    public static final String KEY_PREFIX = "JVR.BB.";

    private Constants(){
        // disable construction
    }

}
