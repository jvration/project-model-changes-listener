/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon.hook;

import com.atlassian.bitbucket.content.ContentService;
import com.atlassian.bitbucket.hook.repository.AsyncPostReceiveRepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHook;
import com.atlassian.bitbucket.hook.repository.RepositoryHookContext;
import com.atlassian.bitbucket.hook.repository.RepositoryHookService;
import com.atlassian.bitbucket.repository.MinimalRef;
import com.atlassian.bitbucket.repository.RefChange;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.setting.RepositorySettingsValidator;
import com.atlassian.bitbucket.setting.Settings;
import com.atlassian.bitbucket.setting.SettingsValidationErrors;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.jvr.bitbucket.addon.JvrPluginException;
import com.jvr.bitbucket.addon.scm.ScmExecutor;
import com.jvr.bitbucket.addon.settings.ListenerPluginSettings;
import com.jvr.bitbucket.addon.settings.SettingsRest;
import org.apache.maven.model.CiManagement;
import org.apache.maven.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jvr.bitbucket.addon.Constants.BRANCH_FULL_NAME_PREFIX;
import static com.jvr.bitbucket.addon.hook.JenkinsPostReceiveHookConstants.CLONE_TYPE;
import static com.jvr.bitbucket.addon.hook.JenkinsPostReceiveHookConstants.IGNORE_CERTS;
import static com.jvr.bitbucket.addon.hook.JenkinsPostReceiveHookConstants.JENKINS_BASE;
import static com.jvr.bitbucket.addon.hook.JenkinsPostReceiveHookConstants.KEY;

/**
 * Monitors changes in pom.xml in the root of master branch
 */
@Component
public class JvrReceiveRepositoryHook implements AsyncPostReceiveRepositoryHook, RepositorySettingsValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(JvrReceiveRepositoryHook.class);

    private static final String POM_FILE = "pom.xml";

    private ScmExecutor scmExecutor;

    @ComponentImport
    private ContentService contentService;

    @ComponentImport
    private RepositoryHookService rhService;

    @ComponentImport
    private PluginSettingsFactory pluginSettingsFactory;

    @Autowired
    public void setScmExecutor(ScmExecutor scmExecutor) {
        this.scmExecutor = scmExecutor;
    }

    @Autowired
    public void setContentService(ContentService contentService) {
        this.contentService = contentService;
    }

    @Autowired
    public void setRhService(RepositoryHookService rhService) {
        this.rhService = rhService;
    }

    @Autowired
    public void setPluginSettingsFactory(PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @Override
    public void postReceive(@Nonnull RepositoryHookContext context, @Nonnull Collection<RefChange> refChanges) {

        ListenerPluginSettings settings = SettingsRest.getPluginConfiguration(pluginSettingsFactory);

        Repository repository = context.getRepository();
        String branch = BRANCH_FULL_NAME_PREFIX + settings.getBranchToWatch();

        for (RefChange refChange : refChanges) {

            MinimalRef minRef = refChange.getRef();
            // change configuration only on changes in master
            if (branch.equals(minRef.getId())) {

                List<String> changedFiles = scmExecutor.getListOfChangedFiles(repository, refChange.getFromHash(), refChange.getToHash());
                if (changedFiles.contains(POM_FILE)) {
                    LOGGER.info("{} file is changed in {} branch", POM_FILE, minRef.getId());
                    String ciServer = getCiServerFromPom(outputStream -> contentService.streamFile(repository, settings.getBranchToWatch(), POM_FILE, type -> outputStream));
                    if (ciServer != null) {
                        configureJenkinsHook(repository, getCiServerRoot(ciServer), settings);
                    } else {
                        LOGGER.info("CI server is not set in {}", POM_FILE);
                    }
                }
            }
        }
    }

    /**
     * Configures and enables Jenkins Hook
     *
     * @param repository to configure the hook for
     * @param ciUrl      to use in the hook configuration
     */
    private void configureJenkinsHook(Repository repository, String ciUrl, ListenerPluginSettings pluginSettings) {

        RepositoryHook hook = rhService.getByKey(repository, KEY);
        if (hook == null) {
            LOGGER.error("Repository hook {} is not found", KEY);
            return;
        }

        Settings settings = rhService.getSettings(repository, KEY);

        Map<String, Object> mapSettings = new HashMap<>();
        if (settings != null) {
            String curr = settings.getString(JENKINS_BASE);
            if (curr != null && curr.equals(ciUrl) && hook.isEnabled()) {
                // the hook is already configured and enabled
                return;
            }
            // the retrieved from 'settings' map is not mutable
            mapSettings.putAll(settings.asMap());
        } else {
            mapSettings.put(CLONE_TYPE, pluginSettings.getSavedRepoUrlProtocol());
            mapSettings.put(IGNORE_CERTS, Boolean.TRUE);
        }
        mapSettings.put(JENKINS_BASE, ciUrl);

        Settings changed = rhService.createSettingsBuilder().addAll(mapSettings).build();
        if (hook.isEnabled()) {
            rhService.setSettings(repository, KEY, changed);
        } else {
            rhService.enable(repository, KEY, changed);
            LOGGER.info("{} web hook is enabled", KEY);
        }

        LOGGER.info("{} web hook is configured", KEY);

    }

    /**
     * Strips off Jenkins "folders" from URL
     *
     * @param ciServer either root or folder URL
     * @return root URL
     */
    public static String getCiServerRoot(String ciServer) {
        if (ciServer == null) {
            return null;
        }
        int index = ciServer.indexOf("/job/");
        if (index > 0) {
            return ciServer.substring(0, index + 1);
        }
        return ciServer;
    }

    /**
     * Retrieve "ciManagement/ciUrl" value from POM file
     *
     * @param inputSource source of POM
     * @return value of "ciManagement/ciUrl" or null
     */
    public String getCiServerFromPom(InputSource inputSource) {

        String ciServer = null;
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {

            inputSource.toOutputStream(outputStream);
            MavenModelExaminer examiner = new MavenModelExaminer(new String(outputStream.toByteArray(), StandardCharsets.UTF_8));

            Model pomModel = examiner.getModel();
            CiManagement ciManagement = pomModel.getCiManagement();
            if (ciManagement != null) {
                String ciUrl = ciManagement.getUrl();
                if (ciUrl != null) {
                    ciServer = MavenValueResolver.resolveValue(examiner, ciUrl);
                }
            }

        } catch (IOException | JvrPluginException e) {
            LOGGER.warn("Was not able to retrieve or understand POM content", e);
        }

        return ciServer;

    }

    @Override
    public void validate(@Nonnull Settings settings, @Nonnull SettingsValidationErrors errors, @Nonnull Repository repository) {
    }


}
