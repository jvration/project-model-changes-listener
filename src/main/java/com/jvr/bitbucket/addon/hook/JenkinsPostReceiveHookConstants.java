/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon.hook;

/**
 * These constants are copied from https://github.com/Nerdwin15/stash-jenkins-postreceive-webhook/blob/master/src/main/java/com/nerdwin15/stash/webhook/Notifier.java
 */
public final class JenkinsPostReceiveHookConstants {

    /**
     * Key for the repository hook
     */
    public static final String KEY = "com.nerdwin15.stash-stash-webhook-jenkins:jenkinsPostReceiveHook";

    /**
     * Field name for the Jenkins base URL property
     */
    public static final String JENKINS_BASE = "jenkinsBase";

    /**
     * Field name for the Repo clone type property
     */
    public static final String CLONE_TYPE = "cloneType";

    /**
     * Field name for the ignore certs property
     */
    public static final String IGNORE_CERTS = "ignoreCerts";

    private JenkinsPostReceiveHookConstants() {
        // disable construction
    }

}
