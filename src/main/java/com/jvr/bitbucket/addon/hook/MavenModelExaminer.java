/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon.hook;

import com.jvr.bitbucket.addon.JvrPluginException;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;

import java.io.StringReader;
import java.util.Properties;

public class MavenModelExaminer implements MavenValueResolver.MavenPropertyProvider {

    private final Model model;

    MavenModelExaminer(String pom) {
        this.model = loadPomModel(pom);
    }

    private Model loadPomModel(String pom) {

        MavenXpp3Reader mavenReader = new MavenXpp3Reader();
        StringReader reader = new StringReader( pom );

        Model model;
        try {
            model = mavenReader.read(reader);
        } catch (Exception e) {
            throw new JvrPluginException("Failed to load Maven model", e);
        }

        return model;

    }

    Model getModel() {
        return model;
    }

    /**
     * Returns property value as it's set in pom.xml
     *
     * @param key property name
     * @return property value
     */
    @Override
    public String getPomProperty(String key) {

        String group = model.getGroupId();
        String artifact = model.getArtifactId();
        String version = model.getVersion();

        String value = MavenValueResolver.derivePropertyFromModuleKey( group, artifact, version, key);

        if (value == null) {
            Properties properties = getAllProperties();
            value = properties.getProperty(key);
        }
        if (value == null) {
            value = key;
        }
        return value;
    }

    /**
     * @return properties of the model, including those from profiles
     */
    private Properties getAllProperties() {

        Properties all = new Properties();

        if (model.getProperties() != null) {
            all.putAll(model.getProperties());
        }

        return all;
    }

}

