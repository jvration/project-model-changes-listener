/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon.hook;

import java.util.Arrays;
import java.util.List;

/**
 * @author viktor@jv-ration.com
 */
class MavenValueResolver {

    private static final String TOKEN_OPEN = "${";
    private static final char TOKEN_CLOSE = '}';

    private MavenValueResolver() {
    }

    private static final List<String> PROJECT_PREFIXES = Arrays.asList("pom.", "project.");

    /**
     * Resolves value found in POM with properties
     *
     * @param pomValue unresolved value
     * @return resolved value
     */
    static String resolveValue(MavenPropertyProvider module, String pomValue) {

        if (pomValue == null) {
            return null;
        }

        // This can also be done using InterpolationFilterReader,
        // but it requires reparsing the file over and over until
        // it doesn't change.
        StringBuilder ret = new StringBuilder();
        String trail = pomValue;
        int idx, idx2;

        while ((idx = trail.indexOf(TOKEN_OPEN)) >= 0) {
            // append prefix to result
            ret.append(trail.substring(0, idx));

            // strip prefix from original
            trail = trail.substring(idx + 2);

            // if no matching } then bail
            idx2 = trail.indexOf(TOKEN_CLOSE);
            if (idx2 < 0) {
                break;
            }

            // strip out the key and resolve it
            // resolve the key/value for the ${statement}
            String nk = trail.substring(0, idx2);
            trail = trail.substring(idx2 + 1);

            String nv = module.getPomProperty(nk);

            // if the key cannot be resolved,
            // leave it alone ( and don't parse again )
            // else prefix the original string with the
            // resolved property ( so it can be parsed further )
            // taking recursion into account.
            if (nv == null) {
                ret.append(TOKEN_OPEN).append(nk).append(TOKEN_CLOSE);
            } else {
                trail = nv + trail;
            }
        }

        String resolve = ret.toString() + trail;

        if (!pomValue.equals(resolve)) {
            resolve = resolveValue(module, resolve);
        }

        return resolve;
    }

    /**
     * Retrieves property values from the module key
     *
     * @param propertyName name of the property to retrieve value
     * @return unresolved value of the property or null
     */
    static String derivePropertyFromModuleKey(String groupId, String artifactId, String version, String propertyName) {

        for (String prefix : PROJECT_PREFIXES) {
            if (propertyName.startsWith(prefix)) {
                return derivePropertyFromModuleKey(groupId, artifactId, version, propertyName.substring(prefix.length()));
            }
        }

        if ("groupId".equals(propertyName)) {
            return groupId;
        } else if ("artifactId".equals(propertyName)) {
            return artifactId;
        } else if ("version".equals(propertyName) && (version != null)) {
            return version;
        }

        return null;

    }

    public interface MavenPropertyProvider {
        String getPomProperty(String key);
    }

}

