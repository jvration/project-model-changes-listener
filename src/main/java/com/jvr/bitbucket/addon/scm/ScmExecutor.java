/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon.scm;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.CommandBuilder;
import com.atlassian.bitbucket.scm.ScmCommandBuilder;
import com.atlassian.bitbucket.scm.ScmService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Utility class for executing GIT commands
 *
 * @author Viktor Sadovnikov viktor@jv-ration.com
 */
@Component
public class ScmExecutor {

    private static final String ZERO_COMMIT = "0000000000000000000000000000000000000000";

    @ComponentImport
    private ScmService scmService;

    @Autowired
    public void setScmService(ScmService scmService) {
        this.scmService = scmService;
    }

    /**
     * Executes given GIT command with provided arguments in the directory
     *
     * @param repository to execute command against to
     * @param execDir directory to execute command from
     * @param command command itself
     * @param args arguments
     *
     * @return command standard output
     */
    public String execute(Repository repository, File execDir, String command, String... args) {

        ScmCommandBuilder scmCommandBuilder = scmService.createBuilder(repository);
        if (execDir != null) {
            scmCommandBuilder.workingDirectory(execDir);
        }

        CommandBuilder builder = scmCommandBuilder.command(command);
        StringBuilder strBuilder = new StringBuilder(command);
        for (String arg : args) {
            builder = builder.argument(arg);
            strBuilder.append(' ').append(arg);
        }

        Object result = builder.build(new GitCommandOutputHandler(strBuilder.toString())).call();
        return (result != null) ? result.toString() : null;
    }

    /**
     * Retrieves lists of changed/added/deleted files by running "git log"
     *
     * @param repository to lookup changes in
     * @param fromHash from commmit id
     * @param toHash to commmit id
     *
     * @return list of relative to the repository root paths to changed files
     */
    public List<String> getListOfChangedFiles(Repository repository, String fromHash, String toHash) {

        String range = fromHash + ".." + toHash;

        if (ZERO_COMMIT.equals(toHash)) {
            // Not interested in analyzing branch deletions.
            return new ArrayList<>();
        }
        if (ZERO_COMMIT.equals(fromHash)) {
            // Adjust range for new branches:
            range = toHash;
        }

        String result = execute(repository, null, "log", "--first-parent", "-m", "-p", "--pretty=", "--name-only", range);
        List<String> changedFiles;
        if (result != null) {
            String[] array = result.split("\n");
            changedFiles = Arrays.asList(array);
        } else {
            changedFiles = new ArrayList<>();
        }
        return changedFiles;
    }

}
