/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon.scm;

import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;

/**
 * Provides access to command console output
 *
 * @author Viktor Sadovnikov viktor@jv-ration.com
 */
public class GitCommandOutputHandler implements CommandOutputHandler<String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(GitCommandOutputHandler.class);
    private final String command;

    private String output;

    GitCommandOutputHandler(String command) {
        this.command = command;
    }

    @Nullable
    @Override
    public String getOutput() {
        return StringUtils.chomp(output);
    }

    @Override
    public void process(InputStream inputStream) throws ProcessException {
        LOGGER.debug("Processing : {}", command);
        try {
            output = IOUtils.toString(inputStream);
            LOGGER.trace(output);
        } catch (IOException e) {
            throw new ProcessException(String.format("Command '%s' failed", command), e);
        }
    }

    @Override
    public void complete() throws ProcessException {
        LOGGER.debug("Completed : {}", command);
    }

    @Override
    public void setWatchdog(Watchdog watchdog) {
    }

}

