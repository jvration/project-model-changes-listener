(function ($) { // this closure helps us keep our variables to ourselves.
// This pattern is known as an "iife" - immediately invoked function expression

    // form the URL
    var url = AJS.contextPath() + "/rest/project-model-changes-listener-settings/1.0/";

    function updateConfig() {

        var stBranchValue = "other";
        if ($("#branch-master").attr("checked") === "checked") {
            stBranchValue = 'master';
        } else if ($("#branch-develop").attr("checked") === "checked") {
            stBranchValue = 'develop';
        }
        var protocol = '"repoUrlProtocol": "' +  $("#protocol").val();
        var stBranch = '", "standardBranch": "' +  stBranchValue;
        var othBranch = '", "otherBranch": "' +  $("#other").val();

        $.ajax({
            cache: false,
            url: url,
            type: "PUT",
            contentType: "application/json",
            data: '{ ' + protocol + stBranch + othBranch + '" }',
            processData: false
        }).done(function() {
            $("#settings-update-message").removeAttr("style");
        }).fail(function() {
            alert( "Failed to update settings on " + url);
        });

    }

    function refreshConfiguration() {
        // request the config information from the server
        $.ajax({
            cache: false,
            url: url,
            dataType: "json"
        }).done(function(config) { // when the configuration is returned...

            // ...populate the form.
            $("#protocol").val(config.repoUrlProtocol);

            otherBrName = $("#other");
            otherBrName.val(config.otherBranch);

            if (config.standardBranch === "master") {
                $("#branch-master").attr("checked", "checked");
                $("#branch-develop").removeAttr("checked");
                $("#branch-other").removeAttr("checked");
                otherBrName.prop("disabled", true);
            } else if (config.standardBranch === "develop") {
                $("#branch-master").removeAttr("checked");
                $("#branch-develop").attr("checked", "checked");
                $("#branch-other").removeAttr("checked");
                otherBrName.prop("disabled", true);
            } else {
                $("#branch-master").removeAttr("checked");
                $("#branch-develop").removeAttr("checked");
                $("#branch-other").attr("checked", "checked");
            }

        }).fail(function() {
            alert( "Failed to retrieve settings from " + url );
        });
    }

    // wait for the DOM (i.e., document "skeleton") to load. This likely isn't necessary for the current case, but may be helpful for AJAX that provides secondary content.
    $(document).ready(function() {

        refreshConfiguration();

        $("#model-change-listener-settings-form").submit(function(e) {
            e.preventDefault();
            updateConfig();
        });

        $('input:radio[name="branch"]').change(
            function(){
                if (this.checked && this.value === 'other') {
                    $("#other").prop("disabled", false)
                } else {
                    $("#other").prop("disabled", true)
                }
            }
        );

    });

})(AJS.$ || jQuery);
