/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ut.com.jvr.bitbucket.addon.hook;

import com.jvr.bitbucket.addon.TestResourceLoader;
import com.jvr.bitbucket.addon.hook.JvrReceiveRepositoryHook;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import static org.junit.Assert.*;

public class JvrReceiveRepositoryHookTest {

    @Test
    public void testStraightCiUrl() {
        assertEquals("http://ci.jv-ration.com:8080/job/decrex/", getCiUrl("straight"));
    }

    @Test
    public void testPropertyCiUrl() {
        assertEquals("http://ci2.jv-ration.com:8080/job/decrex/", getCiUrl("property"));
    }

    @Test
    public void testNestedPropertyCiUrl() {
        assertEquals("http://ci3.jv-ration.com:8080/job/decrex/", getCiUrl("property-nested"));
    }

    private String getCiUrl(String test) {
        JvrReceiveRepositoryHook hook = new JvrReceiveRepositoryHook();
        return hook.getCiServerFromPom(outputStream -> IOUtils.copy(TestResourceLoader.getInputStream("hook-tests/ci-url-" + test + ".xml"), outputStream));
    }

    @Test
    public void testJenkinsFolderUrlNull() {
        assertNull(JvrReceiveRepositoryHook.getCiServerRoot(null));
    }

    @Test
    public void testJenkinsFolderUrl() {
        assertEquals("http://ci.jv-ration.com/", JvrReceiveRepositoryHook.getCiServerRoot("http://ci.jv-ration.com/job/decrex/"));
    }

    @Test
    public void testJenkinsRootUrl() {
        assertEquals("http://ci.jv-ration.com:8080/", JvrReceiveRepositoryHook.getCiServerRoot("http://ci.jv-ration.com:8080/"));
    }

    @Test
    public void testJenkinsFolderUrlWithPath() {
        assertEquals("http://ci.jv-ration.com/jenkins/", JvrReceiveRepositoryHook.getCiServerRoot("http://ci.jv-ration.com/jenkins/job/decrex/"));
    }

    @Test
    public void testJenkinsRootUrlWithPath() {
        assertEquals("http://ci.jv-ration.com:8080/jenkins/", JvrReceiveRepositoryHook.getCiServerRoot("http://ci.jv-ration.com:8080/jenkins/"));
    }

}