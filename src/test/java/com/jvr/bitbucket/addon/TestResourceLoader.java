/*
 * Copyright 2017 Viktor Sadovnikov @ JV-ration.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.jvr.bitbucket.addon;

import java.io.InputStream;

/**
 * @author Viktor Sadovnikov viktor@jv-ration.com
 */
public class TestResourceLoader {

    public static InputStream getInputStream(String path) {
        ClassLoader loader = TestResourceLoader.class.getClassLoader();
        String testPath = path.startsWith("/") ? path.substring(1) : path;
        return loader.getResourceAsStream(testPath);
    }

}
